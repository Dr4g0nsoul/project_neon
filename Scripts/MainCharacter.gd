extends KinematicBody2D

var Player_Line = 0
var btn_pressed = false
export var speed = 600
export var distance = 300
var movement = Vector2(0,0)
var mov_difference
var move_up = false
var move_down = false

#jumping
export var jump_duration = 1.5
export var jumping_delay = 0.1
var max_jump_height = 50
var jump_speed = 1
var original_jump_pos
var jumping = false
onready var jumping_delay_timer = $JumpingDelayTimer
onready var jumping_timer = $JumpingTimer

#sliding
export var slide_duration = 0.7
export var sliding_delay = 0.05
var sliding = false
onready var sliding_delay_timer = $SlideDelayTimer
onready var sliding_timer = $SlideTimer

#flying
export var fly_duration = 10
var fly_scale_speed = 0.2
var fly_height = 300
var fly_up_down_speed = 300
var fly_wave_speed = 200
var fly_original_pos
onready var fly_timer = $FlyAwayTimer
var flying = false
var fly_altitude_reached = false
var fly_altitude
var fly_max = false
var fly_diff = 50
var fly_original_scale
var wanna_fly = false

#speedup
export var speedup_speed = 10
export var speedup_time = 2
onready var speedup_timer = $SpeedupTimer

#knockback variables
export var knockback_time = 1
export var invincibility_time = 2
export var knockback_speed = 50

onready var knockback_timer = $"KnockbackTimer"
onready var invincibility_timer = $InvincibilityTimer
onready var player_sprite = $MainCharacterSprite
onready var player_collision = $MainCharacterShape

onready var game = $"../ObstacleSpawns"

func _ready():
	#knockback timer
	knockback_timer.one_shot = true
	knockback_timer.wait_time = knockback_time
	
	#invincibility timer
	invincibility_timer.one_shot = true
	invincibility_timer.wait_time = invincibility_time
	
	#jumping timers
	jumping_delay_timer.wait_time = jumping_delay
	jumping_delay_timer.one_shot = true
	jumping_timer.wait_time = jump_duration
	jumping_timer.one_shot = true
	
	#sliding timers
	sliding_delay_timer.wait_time = jumping_delay
	sliding_delay_timer.one_shot = true
	sliding_timer.wait_time = jump_duration
	sliding_timer.one_shot = true
	
	#flying timer
	fly_timer.wait_time = fly_duration
	fly_timer.one_shot = true
	
	#speedup timer
	speedup_timer.wait_time = speedup_time
	speedup_timer.one_shot = true
	
	player_sprite.play("running")
	pass


func _physics_process(delta):
	#knockback
	if knockback_timer.is_stopped():
		movement.x = 0
	else:
		movement.x =  -knockback_speed
	#jumping
	if Input.is_action_just_pressed("ui_accept") and not jumping and jumping_delay_timer.is_stopped():
		original_jump_pos = player_sprite.position.y
		player_sprite.play("jumping_up")
		jumping_delay_timer.start()
	
	#sliding
	if Input.is_action_just_pressed("ui_select") and not sliding and sliding_delay_timer.is_stopped():
		player_sprite.play("sliding_down")
		sliding_delay_timer.start()
	
	#speedup
	if not speedup_timer.is_stopped():
		movement.x = speedup_speed
	
	#movement
	if not move_up and not move_down:
		movement.y = 0
		if wanna_fly:
			wanna_fly = false
			fly_original_pos = position.y
			fly_original_scale = player_sprite.scale.x
			fly_timer.start()
			player_sprite.play("sliding_down")
			flying = true
	if not move_up and not move_down and knockback_timer.is_stopped() and not jumping and not sliding and not flying:
		movement.y=0;
		if Input.is_action_just_pressed("ui_up") and Player_Line != -1:
			player_sprite.play("move_up")
			movement.y = -speed
			Player_Line -= 1
			player_sprite.z_index = Player_Line + 2
			#Mask change
			if Player_Line == 0:
				set_collision_mask(4)
			else:
				set_collision_mask(8)
			move_up = true
			mov_difference = position.y - distance
		elif Input.is_action_just_pressed("ui_down") and Player_Line != 1:
			player_sprite.play("move_down")
			movement.y = speed
			Player_Line += 1
			player_sprite.z_index = Player_Line + 2
			#Mask change
			if Player_Line == 0:
				set_collision_mask(4)
			else:
				set_collision_mask(2)
			move_down = true
			mov_difference = position.y + distance
	else:
		if(move_down and position.y >= mov_difference):
			move_up = false
			move_down = false
			player_sprite.play("running")
		if(move_up and position.y <= mov_difference):
			move_up = false
			move_down = false
			player_sprite.play("running")
	if jumping and player_sprite.position.y > original_jump_pos - max_jump_height:
		player_sprite.translate(Vector2(0, -jump_speed))
	
	#flying
	if not fly_timer.is_stopped():
		if player_sprite.scale.x > (-1)*fly_original_scale:
			player_sprite.scale = Vector2(player_sprite.scale.y - fly_scale_speed,player_sprite.scale.y)
		print("Pos y: " + String(position.y) + " > " + String(fly_original_pos - fly_height))
		if not fly_altitude_reached:
			if position.y > fly_original_pos - fly_height:
				movement.y = -fly_up_down_speed
			else: 
				movement.y = 0
				fly_altitude_reached = true
		if fly_altitude_reached:
			fly_altitude = fly_original_pos - fly_height
			if fly_max:
				if position.y <= fly_altitude + fly_diff:
					movement.y = fly_wave_speed
				else:
					fly_max = false
			else:
				if position.y > fly_altitude - fly_diff:
					movement.y = -fly_wave_speed
				else:
					fly_max = true
	elif flying:
		if player_sprite.scale.x < fly_original_scale:
			player_sprite.scale = Vector2(player_sprite.scale.y + fly_scale_speed/50,player_sprite.scale.y)
		if position.y < fly_original_pos:
			movement.y = fly_up_down_speed
		else:
			movement.y = 0
			fly_max = false
			fly_altitude_reached = false
			flying = false
			player_sprite.scale = Vector2(fly_original_scale, player_sprite.scale.y)

	move_and_slide(movement)

func got_hit():
	if not(flying or wanna_fly): 
		game.level_one()
		player_sprite.play("knockback")
		knockback_timer.start()
		match Player_Line:
			-1:
				set_collision_mask_bit(3,false)
			0:
				set_collision_mask_bit(2, false)
			1:
				set_collision_mask_bit(1, false)
		
		invincibility_timer.start()
	#set_collision_layer_bit(0,false)


func _on_KnockbackTimer_timeout():
	player_sprite.play("running")
	#set_collision_layer_bit(0, true)


func _on_InvincibilityTimer_timeout():
	match Player_Line:
		-1:
			set_collision_mask_bit(3, true)
		0:
			set_collision_mask_bit(2, true)
		1:
			set_collision_mask_bit(1, true)


func _on_JumpingDelayTimer_timeout():
	jumping = true
	hitbox_jump()
	jumping_timer.start()


func _on_JumpingTimer_timeout():
	jumping = false
	hitbox_normal()
	player_sprite.play("jumping_down")
	player_sprite.position.y = original_jump_pos

func hitbox_normal():
	player_collision.scale = Vector2(1, 1)
	player_collision.position = Vector2(0, 0)

func hitbox_jump():
	player_collision.scale = Vector2(1, 0.5)
	player_collision.position = Vector2(0, -16)

func hitbox_slide():
	player_collision.scale = Vector2(1, 0.5)
	player_collision.position = Vector2(0, 16)

## Pill effects
func recover():
	speedup_timer.start()

func fly_away_to_victory():
	wanna_fly = true

func _on_SlideTimer_timeout():
	player_sprite.play("sliding_up")
	sliding = false
	hitbox_normal()


func _on_SlideDelayTimer_timeout():
	sliding = true
	hitbox_slide()
	sliding_timer.start()


func _on_MainCharacterSprite_animation_finished():
	if(player_sprite.animation == "jumping_down" or player_sprite.animation == "sliding_up"):
		player_sprite.play("running")


func _on_FlyAwayTimer_timeout():
	invincibility_timer.start()
	player_sprite.play("sliding_up")
