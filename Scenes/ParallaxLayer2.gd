extends ParallaxLayer

var movement = Vector2(0,0)
export var speed = 50

func _ready():
	set_process(true)

func _process(delta):
	movement.x = - speed * delta
	if position.x < -512:
		position.x = 512
	translate(movement)
	
