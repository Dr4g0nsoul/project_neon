extends KinematicBody2D

var speed = 10
var screen_entered = false
var movement = Vector2()

onready var sprite = $Sprite

func _ready():
	movement = Vector2(-speed, 0)
	pass

func _physics_process(delta):
	var collision = move_and_collide(movement)
	if collision and collision.collider.name == "MainCharacter":
		collision.collider.fly_away_to_victory()
		queue_free()


func _on_VisibilityNotifier2D_screen_entered():
	screen_entered = true


func _on_VisibilityNotifier2D_screen_exited():
	if screen_entered:
		queue_free()
