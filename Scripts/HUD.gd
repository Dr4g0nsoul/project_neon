extends CanvasLayer

var scale_factor = [1,2,4,8]

onready var score_label = $Score
onready var game = $"../ObstacleSpawns"
onready var level_label = $Level
onready var mult_label = $Mult

func _ready():
	score_label.text = String(int(game.score))
	level_label.text = String(game.curr_level)
	mult_label.text = String(game.points_speed_map.keys()[game.curr_level - 1]) + "x"

func _process(delta):
	score_label.text = String(int(game.score))
	level_label.text = String(game.curr_level)
	mult_label.text = String(game.points_speed_map.keys()[game.curr_level - 1]) + "x"
