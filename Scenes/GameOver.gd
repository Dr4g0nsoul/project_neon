extends CanvasLayer

var local_highscore = global.highscore
var local_score = global.last_score

onready var score_label = $YourScoreValue
onready var highscore_label = $HighScoreValue

func _ready():
	score_label.text = String(global.last_score)
	highscore_label.text = String(global.highscore)

func _process(delta):
	score_label.text = String(global.last_score)
	highscore_label.text = String(global.highscore)


func _on_RetryButton_pressed():
	get_tree().change_scene("res://Scenes/World.tscn")


func _on_MainMenuButton_pressed():
	get_tree().change_scene("res://Scenes/MainMenu.tscn")
