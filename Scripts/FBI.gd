extends Area2D

onready var game = $"../ObstacleSpawns"

func _on_FBI_body_entered(body):
	if body.name == "MainCharacter":
		global.last_score = game.score
		get_tree().change_scene("res://Scenes/GameOver.tscn")
