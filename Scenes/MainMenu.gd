extends CanvasLayer

onready var start = $StartOption
var is_hovering_start = false

func _process(delta):
	pass


func _on_StartButtonOption_pressed():
	get_tree().change_scene("res://Scenes/World.tscn")


func _on_QuitButtonOption_pressed():
	get_tree().quit()


func _on_TutorialButtonOption_pressed():
	get_tree().change_scene("res://Scenes/TutorialScreen.tscn")


func _on_CreditsButtonOption_pressed():
	get_tree().change_scene("res://Scenes/CreditsScreen.tscn")
