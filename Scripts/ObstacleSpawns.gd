extends Node

export var start_speed = 10
export var points_per_sec = 100
var score = 0
var point_multiplier

var overall_speed
var speed_multiplier

var points_speed_map = {
	1: 1,
	2: 1.3,
	4: 1.6,
	8: 2.0
}

var levels = [1,2,4,8]
var curr_level = 1
var max_level = 4

var obstacles_by_speed = {
	"fast": 15,
	"normal": 10,
	"slow": 5
}

var patterns = {
	"easy_stair": [1,2,4],
	"easy_stair_inverse": [4,2,1],
	"easy_fc_cb": [3,6],
	"easy_cb_fc": [6,1],
	"line": [7]
}

export var start_delay = 5
export var step_delay = 1
export var pattern_delay = 2
export var pill_delay = 3

#Jump percent, the rest is slide
export var probability_jump = 70


const OBSTACLE_SCENE = preload("res://Scenes/Obstacle.tscn")
const OBSTACLE_SLIDE_SCENE = preload("res://Scenes/ObstacleSlide.tscn")
const OBSTACLE_JUMP_SCENE = preload("res://Scenes/ObstacleJump.tscn")
const RECOVER_DRUG_SCENE = preload("res://Scenes/RecoverDrug.tscn")
const MULTIPLIER_DRUG_SCENE = preload("res://Scenes/MultiplierDrug.tscn")
const FLY_DRUG_SCENE = preload("res://Scenes/FlyingDrug.tscn")

onready var spawn_front = $ObstacleFront
onready var spawn_center = $ObstacleCenter
onready var spawn_back = $ObstacleBack

onready var pattern_timer = $PatternTimer
onready var step_timer = $StepTimer
onready var pill_timer = $PillTimer

onready var bgm1 = $"../BGM1"
onready var bgm2 = $"../BGM2"
onready var bgm3 = $"../BGM3"
onready var bgm4 = $"../BGM4"

var started = false
var current_pattern
var step_count = 0

func _ready():
	randomize()
	
	point_multiplier = levels[curr_level-1]
	speed_multiplier = points_speed_map[point_multiplier] 
	
	pattern_timer.one_shot = true
	step_timer.one_shot = true
	pill_timer.one_shot = true
	
	pattern_timer.wait_time = start_delay
	pattern_timer.start()
	
	step_timer.wait_time = step_delay
	
	pill_timer.wait_time = pill_delay

func _process(delta):
	score = score + points_per_sec*point_multiplier*delta
	pass


func _on_StepTimer_timeout():
	spawn_obstacles()


func _on_PatternTimer_timeout():
	step_count = 0
	if not started:
		started = true
		pattern_timer.wait_time = pattern_delay
		pill_timer.start()
	current_pattern = patterns.keys()[randi() % patterns.size()]
	print("Current Pattern: " + current_pattern)
	
	spawn_obstacles()

func spawn_obstacles():
	if step_count >= patterns[current_pattern].size():
		step_timer.stop()
		pattern_timer.start()
	else:
		match patterns[current_pattern][step_count]:
			0:
				pass
			1:
				spawn_enemy(spawn_front, 1)
			2:
				spawn_enemy(spawn_center, 0)
			3:
				spawn_enemy(spawn_front, 1)
				spawn_enemy(spawn_center, 0)
			4:
				spawn_enemy(spawn_back, -1)
			5:
				spawn_enemy(spawn_front, 1)
				spawn_enemy(spawn_back, -1)
			6:
				spawn_enemy(spawn_center, 0)
				spawn_enemy(spawn_back, -1)
			7:
				spawn_line()
		step_count += 1
		step_timer.start()

func spawn_enemy(location, layer):	
	var obstacle = OBSTACLE_SCENE.instance()
	get_parent().get_node("Obstacles").add_child(obstacle)
	obstacle.movement = Vector2(start_speed*speed_multiplier*(-1),0)
	obstacle.position = location.position
	match layer:
		-1:
			obstacle.set_collision_layer_bit(3, true)
			obstacle.sprite.z_index = layer + 2
		0:
			obstacle.set_collision_layer_bit(2, true)
			obstacle.sprite.z_index = layer + 2
		1:
			obstacle.set_collision_layer_bit(1, true)
			obstacle.sprite.z_index = layer + 2

func spawn_pill(location):
	var choice = randi() % 3
	var pill
	match choice:
		0:
			pill = RECOVER_DRUG_SCENE.instance()
		1:
			pill = MULTIPLIER_DRUG_SCENE.instance()
		2:
			pill = FLY_DRUG_SCENE.instance()
	get_parent().get_node("Items").add_child(pill)
	pill.movement = Vector2(start_speed*speed_multiplier*(-1),0)
	pill.position = location.position

func spawn_line():
	var obstacle_chooser = randi() % 100
	var obstacle_type = 0 #avoid
	if obstacle_chooser < probability_jump:
		obstacle_type = 1 #jump
	else:
		obstacle_type = 2 #slide
	var obstacle_count = randi() % 3 + 1
	
	var obstacle_front
	var obstacle_center
	var obstacle_back
	
	
	match obstacle_count:
		1:
			var choice = randi() % 3
			match choice:
				0:
					spawn_line_aux(spawn_front, 1, obstacle_type)
					spawn_line_aux(spawn_center, 0, 0)
					spawn_line_aux(spawn_back, -1, 0)
				1:
					spawn_line_aux(spawn_front, 1, 0)
					spawn_line_aux(spawn_center, 0, obstacle_type)
					spawn_line_aux(spawn_back, -1, 0)
				2:
					spawn_line_aux(spawn_front, 1, 0)
					spawn_line_aux(spawn_center, 0, 0)
					spawn_line_aux(spawn_back, -1, obstacle_type)
		2:
			var choice = randi() % 3
			match choice:
				0:
					spawn_line_aux(spawn_front, 1, obstacle_type)
					spawn_line_aux(spawn_center, 0, obstacle_type)
					spawn_line_aux(spawn_back, -1, 0)
				1:
					spawn_line_aux(spawn_front, 1, 0)
					spawn_line_aux(spawn_center, 0, obstacle_type)
					spawn_line_aux(spawn_back, -1, obstacle_type)
				2:
					spawn_line_aux(spawn_front, 1, obstacle_type)
					spawn_line_aux(spawn_center, 0, 0)
					spawn_line_aux(spawn_back, -1, obstacle_type)
		3:
			spawn_line_aux(spawn_front, 1, obstacle_type)
			spawn_line_aux(spawn_center, 0, obstacle_type)
			spawn_line_aux(spawn_back, -1, obstacle_type)
	pass
	
	

func spawn_line_aux(location, layer, type):
	var obstacle
	match type:
		0:
			obstacle = OBSTACLE_SCENE.instance()
		1:
			obstacle = OBSTACLE_SLIDE_SCENE.instance()
		2:
			obstacle = OBSTACLE_JUMP_SCENE.instance()
	get_parent().get_node("Obstacles").add_child(obstacle)
	obstacle.movement = Vector2(start_speed*speed_multiplier*(-1),0)
	obstacle.position = location.position
	match layer:
		-1:
			obstacle.set_collision_layer_bit(3, true)
			obstacle.sprite.z_index = layer + 2
		0:
			obstacle.set_collision_layer_bit(2, true)
			obstacle.sprite.z_index = layer + 2
		1:
			obstacle.set_collision_layer_bit(1, true)
			obstacle.sprite.z_index = layer + 2
	

func _on_PillTimer_timeout():
	var location = randi() % 3
	match location:
		0:
			spawn_pill(spawn_front)
		1:
			spawn_pill(spawn_center)
		2:
			spawn_pill(spawn_back)
	pill_timer.start()

func level_up():
	if curr_level + 1 <= max_level:
		curr_level += 1
		point_multiplier = levels[curr_level-1]
		speed_multiplier = points_speed_map[point_multiplier]
		match curr_level:
			2:
				bgm2.seek(bgm1.get_playback_position())
				bgm2.play()
			3:
				bgm3.seek(bgm1.get_playback_position())
				bgm3.play()
			4:
				bgm4.seek(bgm1.get_playback_position())
				bgm4.play()

func level_one():
	bgm2.stop()
	bgm3.stop()
	bgm3.stop()
	
	curr_level = 1
	point_multiplier = levels[curr_level-1]
	speed_multiplier = points_speed_map[point_multiplier]