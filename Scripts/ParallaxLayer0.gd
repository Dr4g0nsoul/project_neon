extends ParallaxLayer

var movement = Vector2(0,0)
export var speed = 500

func _ready():
	set_process(true)

func _process(delta):
	if position.x < -1024:
		position.x = 1024
	movement.x = - speed * delta
	translate(movement)
	